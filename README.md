# Contents of Kubernetes

- [Basics Required to Kick Start With Kubernetes](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/Basics/learning-curve.md)

- [Minikube Installation](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/minikube/minikube-installation.md)
  
  - [Bookinfo Application Example](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/minikube/README.md)

- [Namespace](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/Namespace/create-namespace.md)

- [NFS Setup](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/NFS/nfs-setup.md)

- [Create New Users](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/Create%20New%20Users/create-user.md)
