## Follow the steps to create user with limited namespace access  

[CLICK HERE TO CREATE NAMESPACE](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/Namespace/create-namespace.md)

**For sample here we are considering User as USERNAME, Group as DESIGNATION**

### Create the user credentials

- Create a private key for your user
  ```shell
  openssl genrsa -out USERNAME.key 2048
  ```
 
- Create a certificate sign request USERNAME.csr using the private key you just created (USERNAME.key in this example). Make sure you specify your username and group in the -subj section (CN is for the USERNAME and O for the DESIGNATION). As previously mentioned, we will use USERNAME as the name and DESIGNATION as the group:
   ```shell
   openssl req -new -key USERNAME.key -out USERNAME.csr -subj "/CN=USERNAME/O=DESIGNATION"
   ```
   
- Generate the final certificate USERNAME.crt by approving the certificate sign request, USERNAME.csr, you made earlier. Make sure you substitute the CA_LOCATION placeholder with the location of your cluster CA(IN OUR CASE IT IS /etc/kubernetes/pki/). In this example, the certificate will be valid for 365 days:
  ```shell
  openssl x509 -req -in USERNAME.csr -CA /etc/kubernetes/pki/ca.crt -CAkey /etc/kubernetes/pki/ca.key -CAcreateserial -out USERNAME.crt -days 365
  ```
  
- Save both USERNAME.crt and USERNAME.key in a safe location (in this example we will use /home/USERNAME/.certs/).

- Add a new context with the new credentials for your Kubernetes cluster. This example is for a kubeadm cluster but it should be similar for others. Here we are considering the Namespace as the contxt itself:
  ```shell
  kubectl config set-credentials USERNAME --client-certificate=/home/USERNAME/.certs/USERNAME.crt  --client-key=/home/USERNAME/.certs/USERNAME.key
  kubectl config set-context NAMESPACE --cluster=kubernetes --namespace=CHOOSE-YOUR-NAMESPACE --user=USERNAME --password=PASSWORD
  ```
  
- Test the user:
  ```shell
  kubectl --context=NAMESPACE get pods
  ```
  
### Create the role for managing deployments

- Create a role-deployment-manager.yaml file with the content below. In this yaml file we are creating the rule that allows a user to execute several operations on Deployments, Pods and ReplicaSets (necessary for creating a Deployment), which belong to the core (expressed by "" in the yaml file), apps, and extensions API Groups:
  ```shell
  kind: Role
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    namespace: NAMESPACE
    name: deployment-manager
  rules:
  - apiGroups: ["", "extensions", "apps"]
    resources: ["*"] #Choose as per your wish
    verbs: ["*"] #Choose as per your wish
  ```
  
  SAVE AND EXIT
  
  ```shell
  kubectl create -f role-deployment-manager.yaml
  ```
  
### Create the service account for the USERNAME user

```shell
apiVersion: v1
kind: ServiceAccount
metadata:
  name: USERNAME
  namespace: NAMESPACE
```

SAVE AND EXIT

```shell
kubectl create -f serviceaccount.yaml 
```
```shell
kubectl describe serviceaccount USERNAME -n NAMESPACE
```
```shell
kubectl create token USERNAME -n NAMESPACE --duration=999999h
```

SAVE THE TOKEN

### Bind the role to the USERNAME user

- Create a rolebinding-deployment-manager.yaml file with the content below. In this file, we are binding the deployment-manager Role to the User Account USERNAME inside the NAMESPACE namespace:
  ```shell
  kind: RoleBinding
  apiVersion: rbac.authorization.k8s.io/v1
  metadata:
    name: deployment-manager-binding
    namespace: storage-api
  subjects:
  - kind: User
    name: USERNAME
    namespace: NAMESPACE
    apiGroup: ""
  - kind: ServiceAccount
    name: USERNAME
    namespace: NAMESPACE
  roleRef:
    kind: Role
    name: deployment-manager
    apiGroup: ""
  ```
  
  SAVE AND EXIT
  
  ```shell
  kubectl create -f rolebinding-deployment-manager.yaml
  ```
  
### Test the RBAC Rule

```shell
kubectl --context=CONTEXT-NAME run --image bitnami/dokuwiki mydokuwiki
```

### SHARE THE TOKEN WITH THE USER SO THAT THEY CAN ACCESS THE DASHBOARD
