# NFS SETUP

- Kubernetes-Master: NFS Client
- Kubernetes-Worker-1 => NFS Server
- Kubernetes-Worker-2 => NFS Server

## On Worker Node 1

```shell
# Install nfs server
sudo apt install nfs-kernel-server
```

```shell
# Check the status of the firewall and stop it
sudo systemctl status ufw
```
```shell
sudo systemctl disable ufw
```
```shell
sudo systemctl stop ufw
```
```shell
sudo systemctl status ufw
```

```shell
# Restart the nfs server
sudo systemctl restart nfs-config
```
```shell
sudo systemctl restart nfs-kernel-server
```

```shell
# Create directories for sharing
mkdir /kube-storage
```
```shell
mkdir /kube-storage/worker-1
```

```shell
# Give the write and execute permissions
sudo chmod -R o+w /kube-storage/worker-1
```

### Add the following lines in `nano /etc/exports`

```shell
/kube-storage/worker-1        GATEWAY(rw,sync,no_subtree_check,no_root_squash)
```

SAVE and Exit

```shell
# For the sharing changes to take place
exportfs -a
```

```shell
# Restart the nfs server
sudo systemctl restart nfs-config
```
```shell
sudo systemctl restart nfs-kernel-server
```

```shell
# To check the sharing
exportfs
```
```shell
showmount -e
```

## On Worker Node 2

```shell
# Install nfs server
sudo apt install nfs-kernel-server
```

```shell
# Check the status of the firewall and stop it
sudo systemctl status ufw
```
```shell
sudo systemctl disable ufw
```
```shell
sudo systemctl stop ufw
```
```shell
sudo systemctl status ufw
```

```shell
# Restart the nfs server
sudo systemctl restart nfs-config
```
```shell
sudo systemctl restart nfs-kernel-server
```

```shell
# Create directories for sharing
mkdir /kube-storage
```
```shell
mkdir /kube-storage/worker-2
```

```shell
# Give the write and execute permissions
sudo chmod -R o+w /kube-storage/worker-2
```

### Add the following lines in `nano /etc/exports`

```shell
/kube-storage/worker-2        GATEWAY(rw,sync,no_subtree_check,no_root_squash)
```

SAVE and Exit

```shell
# For the sharing changes to take place
exportfs -a
```

```shell
# Restart the nfs server
sudo systemctl restart nfs-config
```
```shell
sudo systemctl restart nfs-kernel-server
```

```shell
# To check the sharing
exportfs
```
```shell
showmount -e
```

## On Master Node: kubeedge-master

```shell
# Install the nfs-common
sudo apt install nfs-common\
```

```shell
# Create the directories initially where to mount. I have kept the path and directories same inorder to avoid any confusions
mkdir /kube-storage
```
```shell
mkdir /kube-storage/worker-1
```
```shell
mkdir /kube-storage/worker-2
```

```shell
# Now mount  directly via command or add in the /etc/fstab
mount -t nfs -o v3 IP-ADDRESS-TIE-A40-1:/kube-storage/worker-1 /kube-storage/worker-1
```
```shell
mount -t nfs -o v3 IP-ADDRESS-TIE-A40-2:/kube-storage/worker-2 /kube-storage/worker-2
```

```shell
# Check if the mounting
df -hT
```

### Also check creating some directories or files in the NFS servers and check in the nfs client as well.

