## Minikube Installation Steps

### Update and Upgrade your system

```shell
apt-get update -y && apt-get upgrade -y
```

### Installtion of Docker => Container Manager

```shell
sudo apt-get install ca-certificates curl gnupg

sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

sudo usermod -aG docker $USER && newgrp docker
```

To verify the docker installation run: `sudo docker run hello-world`

![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/docker-run-test.png)



### Minikube installation

```shell
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

minikube start --driver=docker
```

![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/mini-start.png)


### To make the docker default driver run

`minikube config set driver docker`

### Example to interact with your kubernetes cluster

`kubectl get pods -A`

![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/view-pods.png)

### To Delete Everything from minikube

`minikube delete all --all`

### References

1. [k8s Documentation](https://minikube.sigs.k8s.io/docs/start/)
