## Click here for [Minikube Installation](https://gitlab.com/20a_akhila/kubernetes/-/blob/main/minikube/minikube-installation.md)

## BookInfo Application Setup Procedure

### Download the Minikube Project Files from git


### Start the Minikube

```shell
minikube start --driver=docker
```

![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/mini-start.png)


### Change the directory to bookinfo-app

```shell
cd SDN-CNI-Course-main/minikube/bookinfo-app
```

![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/ch-dir.png)


### Update the /etc/hosts file in the linux based system for the URL to be accessed in the local

- Copy the IP of the cluster

  ```shell
  kubectl cluster-info
  ```

  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/cluster-info.png)

- Paste the Copied Cluster IP in the /etc/hosts
  
  ```shell
  sudo nano /etc/hosts
  ```

    ```shell
  (IP-ADDRESS)    default.bookinfo.com
  ```
  
  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/etc-hosts.png)


### Deploy the Application

- Deployment of the application
  
  ```shell
  kubectl create -f Deployment/details-deploy.yaml
  ```
  ```shell
  kubectl create -f Deployment/ratings-deploy.yaml
  ```
  ```shell
  kubectl create -f Deployment/reviews-deploy.yaml
  ```
  ```shell
  kubectl create -f Deployment/productpage-deploy.yaml
  ```

  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/create-deploy.png)

- Deploy the Services of the Application
  
  ```shell
  kubectl create -f Service/details-svc.yaml
  ```
  ```shell
  kubectl create -f Service/ratings-svc.yaml
  ```
  ```shell
  kubectl create -f Service/reviews-svc.yaml
  ```
  ```shell
  kubectl create -f Service/productpage-svc.yaml
  ```

  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/create-svc.png)

- Check the status of the pods, services, deployment
  
  ```shell
  kubectl get pods -A
  ```
  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/view-pods.png)
  
  ```shell
  kubectl get deploy -A
  ```
  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/view-deploy.png)

  ```shell
  kubectl get svc -A
  ```
  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/view-svc.png)


### Enable External Access to the Application

- Enable the access using bookinfo-ingress.yaml file
  
  ```shell
  kubectl create -f LoadBalancer/bookinfo-ingress.yaml
  ```

  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/create-ingress-bookinfo.png)

- Check the bookinfo ingress
  
  ```shell
  kubectl get ingress bookinfo
  ```

  ![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/view-ingress-bookinfo.png)
  
### Access Your Application

[http://default.bookinfo.com:30075/productpage](http://default.bookinfo.com:30075/productpage)

![image](https://gitlab.com/20a_akhila/kubernetes/-/raw/main/minikube/assets/images/bookinfo-productpage.png)


## Appendix

- To Delete everything (i.e., pods, services, deploy etc.,)
  
  ```shell
  kubectl delete all --all
  ```

- To delete ingressbookinfo
  
  ```shell
  kubectl delete ingress bookinfo
  ```

- To delete the entire minikube cluster
  
  ```shell
  minikube delete --all
  ```
  ```shell
  minikube delete --purge
  ```
  
## References

- [Minikube Cheatsheet](https://cheat.readthedocs.io/en/latest/kubernetes/minikube.html)
- [Kubectl Cheatsheet Reference 1](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)
- [Kubectl Cheatsheet Reference 2](https://www.pluralsight.com/resources/blog/cloud/kubernetes-cheat-sheet)
- [Cheatsheet for Kubernetes Reference 1](https://medium.com/geekculture/cheatsheet-for-kubernetes-minikube-kubectl-5500ffd2f0d5)
- [Kubernetes Cheat Sheet Reference 2](https://intellipaat.com/blog/tutorial/devops-tutorial/kubernetes-cheat-sheet/)
- [Git Cheatsheet](https://github.com/networked-systems-iith/SDN-CNI-Course/assets/24610167/f8106909-4204-4042-a2a5-b58e65735852)
- [Linux Commands Cheatsheet](http://www.cheat-sheets.org/saved-copy/ubunturef.pdf)
