# Onboarding-Training-for-New-Joinee

## Week-1:

- [ ] Basic Linux Administration
- [ ] What are Monolith and MicroServices
- [ ] LXD Containers in Linux
  * Tasks:
    - [ ] Create an LXD profile (Name: Test) and storage space for it
    - [ ] Create an LXD container under the profile test with latest ubuntu Image
- [ ] What are containers and Virtal Machines and their differences
- [ ] What is Docker and Docker Hub
  * Tasks:
    - [ ] Install Oracle VM VirtualBox and create ubuntu latest OS in it
    - [ ] Install and setup docker in your local
    - [ ] Create a Dockerfile with ubuntu latest image 
      - [ ] Include nginx/apache in the Dockerfile
      - [ ] Create the docker image for the Dockerfile you have created
      - [ ] Upload the new image you have created onto the dockerHub
      - [ ] Deploy the docker image on a new container by running it on 8080 or 5000 port
      - [ ] Open the localhos:8080 or localhost:5000 in your web browser and see if apache/nginx page is displayed
      
## Week-2:

- [ ] What is Edge Cloud
- [ ] What is Kubernetes and What is it's importance when it comes to microservices
* Kubernetes Tasks:
  - [ ] **COMPULSORY**: Complete the Kubernetes for beginners on Udemy <https://www.udemy.com/course/learn-kubernetes/>
  - [ ] Install and setup the microK8 in your local machine
  - [ ] Create a pod with the image you have created by pulling it from dockerHub
  - [ ] Create a deployement and scale the pod by creating 2 replica sets of the same pod
  - [ ] Create a service and expose the API so that we can view the apache/nginx in the web browser
  - [ ] Create a Kubernetes dashboard and view your pod in the GUI
- [ ] Reading the Edge Cloud experience paper(Title: "Experience: Implementation of Edge-Cloud for Autonomous Navigation Applications"
) written by our team to understand our work! <https://ieeexplore.ieee.org/document/10041370> 

## References:

* #### Linux Administration:
  * [Linux System Administration Basics](https://www.linode.com/docs/guides/linux-system-administration-basics/)
  * [Learn the Basics of the Linux Operating System](https://www.freecodecamp.org/news/learn-the-basics-of-the-linux-operating-system/)
  * For Furture: [Introduction to Linux](https://www.freecodecamp.org/news/introduction-to-linux/)
  
* #### MonoLith and MicroServices:
  * [What are microservices?](https://www.ibm.com/topics/microservices#:~:text=The%20difference%20between%20microservices%20and,a%20large%2C%20tightly%20coupled%20application.)
  * [Microservices vs. monolithic architecture](https://www.atlassian.com/microservices/microservices-architecture/microservices-vs-monolith)
  
* #### LXD Containers in Linux:
  * [LXD Introduction](https://linuxcontainers.org/lxd/introduction/)
  
* #### Containers and Virtual Machines:
  * [Containers vs. Virtual Machines (VMs): What’s the Difference?](https://www.ibm.com/cloud/blog/containers-vs-vms)
  * [Download Oracle VirtualBox](https://www.virtualbox.org/wiki/Downloads)
  
* #### Docker and DockerHub:
  * [Get Started with Docker](https://docs.docker.com/get-started/)
  * [Docker CLI Cheat Sheet](https://docs.docker.com/get-started/docker_cheatsheet.pdf)
  
* #### Edge Cloud:
  * [Cloud vs. edge](https://www.redhat.com/en/topics/cloud-computing/cloud-vs-edge)
  * [What is edge computing?](https://www.ibm.com/cloud/what-is-edge-computing)
  * [Edge Cloud Operations: A Systems Approach](https://ops.systemsapproach.org/)
  
* #### Kubernetes:
  * **MANDATORY**: [Click here to complete this course and get the certification](https://www.udemy.com/course/learn-kubernetes/). We will refund the course amount. 
  
* #### Papers: 
  * [Experience: Implementation of Edge-Cloud for Autonomous Navigation Applications](https://ieeexplore.ieee.org/document/10041370)
  * [From Clouds to Hybrid Clouds](https://www.acm.org/binaries/content/assets/india/acm-india-minigraph-mar2023.pdf)
  * [Democratizing the Network Edge](https://opennetworking.org/wp-content/uploads/2019/10/ccr.pdf)
  * [OpenRTiST: End-to-End Benchmarking for Edge Computing](https://ieeexplore.ieee.org/document/9229154)
  * [Large-scale cluster management at Google with Borg](https://static.googleusercontent.com/media/research.google.com/en//pubs/archive/43438.pdf)

* #### Books and Resources:
  * [Kubernetes: Up and Running Dive into the Future of Infrastructure. O’Reilly Media, Inc.,1st edition, 2017](https://cloudflare-ipfs.com/ipfs/bafykbzacedhaacmvojvvyabjkpugxsnqovcwmlqvq3tdpcr5joss7sklmbweo?filename=Kelsey%20Hightower%2C%20Brendan%20Burns%2C%20Joe%20Beda%20-%20Kubernetes_%20Up%20and%20Running_%20Dive%20into%20the%20Future%20of%20Infrastructure-O%E2%80%99Reilly%20Media%20%282017%29.pdf)
